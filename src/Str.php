<?php namespace pPort\Str;

/**
 * pPort Str Utility
 *
 * Collection of functions to manipulate/work with strings
 *
 * @author Martin Muriithi <martin@pporting.org>
 * @package system.utils
 * @category utils
 * @link http://www.pporting.org/docs/system/utils/string
 * @copyright Copyright (c) pPort Community Team
 * @license http://www.pporting.org/license
 * @version 1.0.0 <script version>
 * @since pPort Version 1.0.0
 *
 */

class Str{
    /**
    * is_url
    *
    * Checks if a string is a url
    *
    * @param <string> $string <A string to be checked>
    *
    * @return <boolean>
    */
    public function is_url($string)
    {
        if(filter_var(trim($string), FILTER_VALIDATE_URL))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
    * with_urls
    *
    * Returns a string with its linked urls
    *
    * @param <string> $string <A string to be parsed>
    *
    * @return <string>
    */
    public function with_urls($text,$attrs=array())
    {
        $reg_exUrl ="/((((http|https|ftp|ftps)\:\/\/)|www\.)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(\/\S*)?)/";
        if(is_array($attrs)&&!empty($attrs))
        {
            array_walk($attrs, create_function('&$i,$k','$i=" $k=\"$i\"";'));
            $attrs = implode($attrs,"");
        }
        else
        {
            $attrs="";
        }
        return preg_replace( $reg_exUrl,"<a href=\"$1\" ".$attrs.">$1</a>", $text );
    }


    /**
    * highlight_php
    *
    * Returns a syntax highlighted version of the given PHP
    *
    * @param <string> $string <The php code string to highlight>
    *
    * @return <string> <The highlighted version of the php code>
    */
    public function highlight_php($string)
    {
        return highlight_string($string);
    }

    /**
     * count_words
     *
     * Returns a count of the words in a string
     *
     *@param     <string> $string <The string to count>
     *@return    <int> <The number of words in the string>
     *
     */
    public  function count_words($string)
    {
        return str_word_count($string);
    }

    /**
     * difference
     *
     * Tells the difference in two strings
     *
     *@param     <string> $string1 <The first string>
     *@param     <string> $string2 <The string to check against>
     *@return    <int> <The character difference in the two words>
     *
     */
    public  function difference($string1,$string2)
    {
        return levenshtein($string1,$string2);
    }

    /**
     * php_strip
     *
     * Returns the PHP source code in filename without PHP comments and whitespace removed.
     *
     *@param     <string> $file_name <The php file>
     *@return    <int> <The php source code in filename without PHP comments and whitespace >
     *
     */
    public  function php_strip($file_name)
    {
        return php_strip_whitespace($file_name);
    }

    /**
     * between
     * 
     * Returns a string  between two specified characters in the string
     *
     *@param  <string> $content <The string to check>
     *@param  <string> $start <The first character>
     *@param  <string> $end <The second character>
     *@return <string> <The extracted string>
     *
     */
    public  function between($string,$start,$end)
    {
        $r = explode($start,$string);
        if (isset($r[1]))
        {
            $r = explode($end, $r[1]);
            return $r[0];
        }
        return '';
    }

    /**
     * extract_emails
     *
     * Extracts emails from a string
     *
     * @param  <string> $string <The string to check>
     * @return <array> <Array of extracted emails>
     *
     */
    public  function extract_emails($string)
    {
        $regexp = '/([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+/i';
        preg_match_all($regexp, $string, $m);
        return isset($m[0]) ? $m[0] : array();
    }

    /**
     * starts_with
     *
     * Check if a string starts with certain characters
     *
     * @param  <string> $string <The string to be checked>
     * @param  <string> $string <The portion of string to check>
     * @return <boolean>
     *
     */
    public function starts_with($string, $needle)
    {
        $length = strlen($needle);
        return (substr($string, 0, $length) === $needle);
    }

    /**
     * ends_with
     *
     * Check if a string ends with certain characters
     *
     * @param  <string> $string <The string to be checked>
     * @param  <string> $string <The portion of string to check>
     * @return <boolean>
     *
     */
    public  function ends_with($haystack, $needle)
    {
        return substr($haystack, -strlen($needle))===$needle;
    }


    /**
     * to_phone_code
     *
     * Ensures a phone number is padded with the proper phone code
     *
     * @param  <string> $string <The string to be checked>
     * @param  <string> $string <The phone-code>
     * @return <boolean>
     *
     */
    public function to_phone_code($phone,$phone_code='254')
    {
        $phone=ltrim($phone,"+");
        if($this->starts_with($phone,'7'))
        {
            $phone=substr_replace($phone, $phone_code, 0,0);
        }

        return $this->starts_with($phone,$phone_code)?$phone:substr_replace($phone,$phone_code, 0, 1);
    }


    /**
     * has
     *
     * Check if a portion of string exists
     *
     * @param  <string> $stack <The string to be checked>
     * @param  <string> $string <The portion of string to check>
     * @return <boolean>
     *
     */
    public function has($stack,$string)
    {
        if(strrpos($stack,$string)!==false)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * truncate
     *
     * Truncate a string to a certain length
     *
     * @param  <string> $string <The string to be truncated>
     * @param  <string> $width <The width after which to truncate>
     * @return <boolean>
     *
     */
    public function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }

    /**
     * truncate
     *
     * Truncate a string to a certain length
     *
     * @param  <string> $string <The string to be truncated>
     * @param  <string> $words <The width after which to truncate>
     * @return <boolean>
     *
     */
    public  function truncate_words($string,$words=20)
    {
        return preg_replace('/((\w+\W*){'.($words-1).'}(\w+))(.*)/', '${1}', $string);
    }


    /**
     * to_words
     *
     * Convert number digits to Words
     *
     * @param  <string> $number <The number to be converted>
     * @return <boolean>
     *
     */
    public function to_words($number) {

        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }
}